def check(n):
    for i in range(1,21):
        if n % i !=0: return False
    return True

n=20
def smallest_multiple(n):
    while True:
        if check(n): return n
        n+=20
print(smallest_multiple(n))